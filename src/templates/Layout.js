import React, { PureComponent } from 'react';
import { Form, Radio, Icon } from 'semantic-ui-react';
import TemplatePreview from './TemplatePreview';
import {
  REGEX__GSM,
  REGEX__GSM2,
  REGEX__LATIN1_GSM,
  REGEX__UNICODE,
} from '../shared/Constants';

class Layout extends PureComponent {
  state = {
    content: '',
    messageLength: 0,
    maxLength: 1600,
    messageType: '',
    smsParts: 1,
    templatePreviewTarget: 'Android',
  };

  handleChange = (event) => {
    const value = event.target.value;
    const length = value.length;
    const isGsm = REGEX__GSM.test(value);
    const isGsm2 = value.match(REGEX__GSM2);
    const isLatin1 = REGEX__LATIN1_GSM.test(value);
    const isUnicode = REGEX__UNICODE.test(value);
    this.setState({ content: value, error: false }, () => {
      let doubleLength = 0;
      if (isGsm && isGsm2) {
        doubleLength = isGsm2.length;
      }
      if (isGsm) {
        this.setState({
          messageLength: length + doubleLength,
          maxLength: 10 * 160,
          messageType: 'GSM7',
          smsParts:
            length + doubleLength !== 0
              ? Math.ceil((length + doubleLength) / 160)
              : 1,
        });
      } else if (isLatin1) {
        this.setState({
          messageLength: length,
          maxLength: 10 * 140,
          messageType: 'Latin-1',
          smsParts: length !== 0 ? Math.ceil(length / 140) : 1,
        });
      } else if (isUnicode) {
        this.setState({
          messageLength: length,
          maxLength: 10 * 70,
          messageType: 'Unicode',
          smsParts: length !== 0 ? Math.ceil(length / 70) : 1,
        });
      } else {
        this.setState({
          messageLength: length,
          maxLength: 10 * 70,
          messageType: 'Unidentified',
          smsParts: length !== 0 ? Math.ceil(length / 70) : 1,
        });
      }
    });
  };

  handleTemplatePreviewTargetChange = () => {
    this.setState({
      templatePreviewTarget:
        this.state.templatePreviewTarget === 'Android' ? 'iOS' : 'Android',
    });
  };

  render() {
    const { content, messageLength, maxLength, smsParts, messageType, templatePreviewTarget } = this.state;

    return (
      <div className="ui fluid container">
        <div className="ui two column stackable doubling grid">
          <div className="column">
            <div className="ui segment">
              <Form size="small">
                <Form.Field>
                  <Form.TextArea 
                    label='Message content' 
                    placeholder='Tell us more about you...'
                    value={this.state.content}
                    onChange={this.handleChange}
                    maxLength={this.state.maxLength}
                    ref={this.contentBoxRef}
                  />
                  <span>
                    Characters: {messageLength} /{' '}
                    {maxLength}
                    &nbsp;&nbsp;&nbsp;&nbsp; SMS parts:{' '}
                    {smsParts} / 10
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    {messageType !== 'Unidentified' &&
                      messageType !== 'GSM7' &&
                      messageLength !== 0 && (
                        <span>
                          {' '}
                          {this.state.messageType} characters
                          detected!{' '}
                        </span>
                      )}
                  </span>
                </Form.Field>
              </Form>
            </div>
          </div>
          <div className="column">
            <div className="row">
              <div className="sixteen wide center aligned column">
                <Form>
                  <Form.Group inline>
                    <Form.Field>
                      <label>
                        <Icon name="mobile alternate" size="large" />
                        Preview:
                      </label>
                    </Form.Field>
                    <Form.Field>
                      <Radio
                        label="Android"
                        name="templatePreviewRadioGroup"
                        value="android"
                        checked={templatePreviewTarget === 'Android'}
                        onChange={this.handleTemplatePreviewTargetChange}
                      />
                    </Form.Field>
                    <Form.Field>
                      <Radio
                        label="iOS"
                        name="templatePreviewRadioGroup"
                        value="ios"
                        checked={templatePreviewTarget === 'iOS'}
                        onChange={this.handleTemplatePreviewTargetChange}
                      />
                    </Form.Field>
                  </Form.Group>
                </Form>
              </div>
            </div>
            <div className="ui two column stackable grid">
              <div className="center aligned column">
                <TemplatePreview
                  message={content}
                  target={templatePreviewTarget}
                  view="home"
                />
              </div>
              <div className="center aligned column">
                <TemplatePreview
                  message={content}
                  target={templatePreviewTarget}
                  view="messages"
                />
              </div>
            </div>
            <div className="row">
              <div className="sixteen wide center aligned column">
                <p>
                  Intended for preview only. Actual results will vary due
                  to different device sizes and mobile messaging
                  applications.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Layout;


