import React from 'react';
import { PropTypes } from 'prop-types';
import styled from 'styled-components';
import { format } from 'date-fns';

// for Android UI set
import AndroidPhone from './preview/mobile/android/AndroidPhone';
import AndroidMenuBar from './preview/mobile/android/AndroidMenuBar';
import AndroidSubMenuBar from './preview/mobile/android/AndroidSubMenuBar';
import AndroidNotificationTitle from './preview/mobile/android/AndroidNotificationTitle';
import AndroidAvatar from './preview/mobile/android/AndroidAvatar';
import AndroidAdd from './preview/mobile/android/AndroidAdd';
import AndroidSendIcon from './preview/mobile/android/AndroidSendIcon';

// for iOS UI set
import IPhone from './preview/mobile/ios/IPhone';
import AppleMessagesIcon from './preview/mobile/ios/AppleMessagesIcon';
import AppleInput from './preview/mobile/ios/AppleInput';
import AppleAppBar from './preview/mobile/ios/AppleAppBar';
import AppleMenuBar from './preview/mobile/ios/AppleMenuBar';
import AppleMenuBarSimple from './preview/mobile/ios/AppleMenuBarSimple';

const DEFAULT_PREVIEW_MESSAGE =
  'Start typing in template field to view preview.';

const TemplatePreviewStyles = styled.div`
  svg {
    max-height: 525px;
  }
`;

const AndroidStyles = styled.div`
  @import url('https://fonts.googleapis.com/css?family=Roboto');
  font-family: 'Roboto', sans-serif;
  font-size: 0.55em;

  .home-view {
    height: 290px;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    background: linear-gradient(
      321deg,
      rgba(2, 0, 36, 1) 0%,
      rgba(81, 81, 161, 1) 41%,
      rgba(95, 133, 191, 1) 54%,
      rgba(0, 212, 255, 1) 100%
    );

    & .menubar {
      font-size: 0.75em;
      text-align: left;
      padding: 0 1em;

      & .home {
        svg {
          margin: 0.25em;
          width: 100%;
          padding: 0.5em;
        }
      }

      & .messages {
        background-color: #f8f8f8;
        color: #333;
        padding: 0 0.75em;
      }
    }

    .header {
      padding: 2em 0;
      color: #fff;
      font-weight: 200;

      & .time {
        margin: 0.2em 0;
        font-size: 3.75em;
      }

      & .date {
        font-size: 1.15em;
      }
    }

    & .notification {
      flex-grow: 2;
      margin: 0.25em;
      text-align: left;
      color: #333;

      & .content {
        background-color: rgba(255, 255, 255, 0.75);
        box-shadow: 0 0 1px 0 rgba(0, 0, 0, 0.21),
          0 2px 1px 0 rgba(0, 0, 0, 0.12);
        overflow: hidden;
        padding: 0.05em 0.5em;
        margin: 0.5em;
        border-radius: 4px;

        & .title {
          height: 15px;

          & svg {
            width: 50%;
          }
        }

        & .contact {
          font-weight: 600;
          height: 12px;
          line-height: 12px;
          font-size: 7px;
          padding-left: 1px;
        }

        & .message-body {
          display: flex;
          justify-content: space-between;
          align-items: center;
          overflow: hidden;
          margin-bottom: 0.5em;

          & .message {
            align-self: flex-start;
            margin: 0.25em;
            line-height: 1.25;
            word-break: break-word;
            overflow-wrap: break-word;
            letter-spacing: 0;
            overflow: hidden;
            color: #5e5e5e;
            min-height: 10px;
            max-height: 18px;
          }

          & .avatar {
            flex-shrink: 0;
            align-self: baseline;
            margin-left: 0.35em;
            width: 15px;
            height: 15px;
          }
        }
      }
    }

    & .footer {
      height: 25px;
      display: flex;
      justify-content: center;

      & .dot {
        width: 4px;
        height: 4px;
        border-radius: 50%;
        background: rgba(255, 255, 255, 0.5);
        margin: 0.5em;
      }

      & .dot-solid {
        background: rgba(255, 255, 255, 1);
        width: 4px;
        height: 4px;
        border-radius: 50%;
        margin: 0.5em;
      }
    }
  }

  .messages-view {
    height: 290px;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    background-color: #f8f8f8;

    & .menubar {
      font-size: 0.75em;
      text-align: left;
      background-color: #fff;

      & .messages {
        & .top {
          height: 15px;

          svg {
            width: 100%;
            padding: 6px 1.25em 4px;
          }
        }

        & .sub {
          height: 25px;
          box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.25);

          & svg {
            width: 100%;
            padding: 0;
          }
        }
      }
    }

    & .message {
      flex-grow: 2;
      display: flex;
      flex-direction: row;
      align-items: flex-end;

      .profile {
        flex-shrink: 0;
        width: 20px;
        height: 20px;
        margin: 0.75em;

        & svg {
          border: none;
        }
      }

      .content {
        text-align: left;
        color: #333;
        background-color: #e0e0e0;
        border-radius: 8px;
        padding: 0.5em;
        margin-right: 1.25em;
        margin-bottom: 0.75em;

        & .message-body {
          margin: 0.25em;
          max-height: 195px;
          overflow: hidden;

          & .message {
            line-height: 1.25;
            word-wrap: break-word;
            word-break: break-word;
            color: #5e5e5e;
          }
        }
      }
    }

    & .footer {
      height: 30px;

      & .compose {
        display: flex;
        width: 100%;
        padding: 0.5em 1em;

        & .add {
          width: 20px;
          height: 20px;
          margin-right: 0.5em;
        }

        & .field {
          flex-grow: 2;
          height: 20px;
          border: 1px solid #e0e0e0;
          border-radius: 2em;
          text-align: left;
          color: #aaa;
          padding-left: 0.5em;
          display: flex;
          justify-content: space-between;

          & .send {
            padding-top: 1px;

            & svg {
              fill: #aaa;
              height: 15px;
              width: 15px;
            }
          }
        }
      }
    }
  }
`;

const IOSStyles = styled.div`
  font-family: system, -apple-system, BlinkMacSystemFont, 'Segoe UI',
    'Helvetica Neue', 'Lucida Grande', sans-serif;
  font-size: 0.55em;

  .home-view {
    height: 315px;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    background: linear-gradient(to right, #654ea3, #eaafc8);

    & .menubar {
      color: #fff;
      background-color: transparent;

      & .home {
        & svg {
          width: 92%;
          height: 20px;
        }
      }
    }

    .header {
      padding: 2em 0;
      color: #fff;

      & .time {
        margin: 0.2em 0;
        font-size: 4em;
        letter-spacing: -0.57px;
        font-weight: 200;
      }

      & .date {
        font-weight: 200;
        font-size: 1.35em;
        letter-spacing: 0.35px;
      }
    }

    & .notification {
      flex-grow: 2;
      margin: 0.75em;
      text-align: left;
      color: #333;

      .content {
        background: rgba(255, 255, 255, 0.6);
        border-radius: 6px;
        padding-bottom: 0.25em;
      }

      & .title {
        padding: 0 1em;
        font-weight: 400;
        line-height: 2em;
        color: #8a8a8f;
        letter-spacing: -0.08px;
        height: 13px;
        display: flex;

        & .app-icon svg {
          border: none;
          height: 15px;
          padding-top: 4px;
        }

        & .messages {
          flex-grow: 2;
          font-size: 6px;
          padding-top: 2px;
          padding-left: 2px;
        }

        & .timestamp {
          float: right;
          padding-top: 2px;
          font-size: 5px;
        }
      }

      & .message-body {
        padding: 0.5em 1em;
        max-height: 38px;
        overflow: hidden;

        & .contact {
          color: #000000;
          letter-spacing: -0.24px;
          line-height: 11px;
          height: 12px;
          font-size: 7px;
        }

        & .message {
          max-height: 18px;
          font-weight: 500;
          color: #111;
          line-height: 1.25em;
          letter-spacing: -0.08px;
          overflow: hidden;
          word-break: break-word;
        }
      }
    }

    & .footer {
      height: 4px;
      color: #fff;
      display: flex;
      justify-content: center;
      margin-bottom: 2px;

      & .beam {
        border-radius: 3em;
        width: 50px;
        height: 2px;
        background-color: rgba(255, 255, 255, 0.75);
      }
    }
  }

  .messages-view {
    height: 315px;
    display: flex;
    flex-direction: column;
    justify-content: space-between;

    & .menubar {
      color: #242424;
      background-color: #f9f9f9;
      margin-bottom: 1em;

      & .messages {
        height: 50px;
      }

      & svg {
        width: 100%;
        height: 50px;
      }
    }

    & .message {
      flex-grow: 2;
      display: flex;
      flex-direction: row;
      align-items: flex-start;

      & .content {
        background-color: #e5e6ea;
        margin-left: 1em;
        margin-right: 4em;
        border-radius: 8px;
        overflow: hidden;

        & .message-body {
          padding: 0.5em 1em;
          text-align: left;
          max-height: 203px;

          & .message {
            color: #111;
            line-height: 1.25em;
            letter-spacing: -0.08px;
            overflow: hidden;
            word-break: break-word;
          }
        }
      }
    }

    & .footer {
      display: flex;
      flex-direction: column;

      & .input {
        height: 16px;

        & svg {
          height: 20px;
          width: 100%;
        }
      }

      & .app-bar {
        height: 28px;

        & svg {
          width: 100%;
        }
      }

      & .beam {
        display: flex;
        align-self: center;
        border-radius: 3em;
        width: 50px;
        height: 2px;
        background-color: rgba(0, 0, 0, 0.75);
        margin-bottom: 4px;
      }
    }
  }
`;

const DeviceMenuBar = (props) => {
  return (
    <section className="menubar">
      {props.device === 'android' && (
        <div className={props.view === 'home' ? 'home' : 'messages'}>
          {props.view === 'home' && <AndroidMenuBar color="white" />}
          {props.view === 'messages' && (
            <React.Fragment>
              <div className="top">
                <AndroidMenuBar />
              </div>
              <div className="sub">
                <AndroidSubMenuBar />
              </div>
            </React.Fragment>
          )}
        </div>
      )}
      {props.device === 'ios' && (
        <div className={props.view === 'home' ? 'home' : 'messages'}>
          {props.view === 'home' && <AppleMenuBarSimple />}
          {props.view === 'messages' && (
            <AppleMenuBar time={format(new Date(), 'h:mm')} />
          )}
        </div>
      )}
    </section>
  );
};

const DeviceHeader = (props) => {
  return (
    <section className="header">
      {props.device === 'android' && (
        <React.Fragment>
          <div className="time">{format(new Date(), 'h:mm')}</div>
          <div className="date">{format(new Date(), 'E, MMM M')}</div>
        </React.Fragment>
      )}
      {props.device === 'ios' && (
        <React.Fragment>
          <div className="time">{format(new Date(), 'h:mm')}</div>
          <div className="date">{format(new Date(), 'EEEE, d MMMM')}</div>
        </React.Fragment>
      )}
    </section>
  );
};

const DeviceMessagePreview = (props) => {
  return (
    <section className="notification">
      {props.device === 'android' && (
        <div className="content">
          <div className="title">
            <AndroidNotificationTitle />
          </div>
          <div className="message-body">
            <div className="message">{props.message}</div>
            <div className="avatar">
              <AndroidAvatar />
            </div>
          </div>
        </div>
      )}
      {props.device === 'ios' && (
        <div className="content">
          <div className="title">
            <div className="app-icon">
              <AppleMessagesIcon />
            </div>
            <div className="messages">MESSAGES</div>
            <div className="timestamp">now</div>
          </div>
          <div className="message-body">
            <div className="message">{props.message}</div>
          </div>
        </div>
      )}
    </section>
  );
};

const DeviceMessage = (props) => {
  return (
    <section className="message">
      {props.device === 'android' && (
        <React.Fragment>
          <div className="profile">
            <AndroidAvatar />
          </div>
          <div className="content">
            <div className="message-body">
              <div className="message">{props.message}</div>
            </div>
          </div>
        </React.Fragment>
      )}
      {props.device === 'ios' && (
        <div className="content">
          <div className="message-body">
            <div className="message">{props.message}</div>
          </div>
        </div>
      )}
    </section>
  );
};

const DeviceFooter = (props) => {
  return (
    <section className="footer">
      {props.device === 'android' && (
        <React.Fragment>
          {props.view === 'home' && (
            <React.Fragment>
              <div className="dot" />
              <div className="dot-solid" />
              <div className="dot" />
            </React.Fragment>
          )}
          {props.view === 'messages' && (
            <div className="compose">
              <div className="add">
                <AndroidAdd />
              </div>
              <div className="field">
                <span style={{ display: 'flex', alignItems: 'center' }}>
                  Text message
                </span>
                <div className="send">
                  <AndroidSendIcon />
                </div>
              </div>
            </div>
          )}
        </React.Fragment>
      )}
      {props.device === 'ios' && (
        <React.Fragment>
          {props.view === 'home' && <div className="beam" />}
          {props.view === 'messages' && (
            <React.Fragment>
              <div className="input">
                <AppleInput />
              </div>
              <div className="app-bar">
                <AppleAppBar />
              </div>
              <div className="beam" />
            </React.Fragment>
          )}
        </React.Fragment>
      )}
    </section>
  );
};

const TemplatePreview = (props) => {
  return (
    <TemplatePreviewStyles>
      {props.target === 'Android' && (
        <AndroidPhone>
          <AndroidStyles>
            {props.view === 'home' && (
              <div className="home-view">
                <DeviceMenuBar device="android" view={props.view} />
                <DeviceHeader device="android" />
                <DeviceMessagePreview
                  device="android"
                  message={props.message || DEFAULT_PREVIEW_MESSAGE}
                />
                <DeviceFooter device="android" view="home" />
              </div>
            )}
            {props.view === 'messages' && (
              <div className="messages-view">
                <DeviceMenuBar device="android" view={props.view} />
                <DeviceMessage
                  device="android"
                  message={props.message || DEFAULT_PREVIEW_MESSAGE}
                />
                <DeviceFooter device="android" view="messages" />
              </div>
            )}
          </AndroidStyles>
        </AndroidPhone>
      )}
      {props.target === 'iOS' && (
        <IPhone>
          <IOSStyles>
            {props.view === 'home' && (
              <div className="home-view">
                <DeviceMenuBar device="ios" view={props.view} />
                <DeviceHeader device="ios" />
                <DeviceMessagePreview
                  device="ios"
                  message={props.message || DEFAULT_PREVIEW_MESSAGE}
                />
                <DeviceFooter device="ios" view={props.view} />
              </div>
            )}
            {props.view === 'messages' && (
              <div className="messages-view">
                <DeviceMenuBar device="ios" view={props.view} />
                <DeviceMessage
                  device="ios"
                  message={props.message || DEFAULT_PREVIEW_MESSAGE}
                />
                <DeviceFooter device="ios" view={props.view} />
              </div>
            )}
          </IOSStyles>
        </IPhone>
      )}
    </TemplatePreviewStyles>
  );
};

TemplatePreview.propTypes = {
  message: PropTypes.string,
  target: PropTypes.string,
  view: PropTypes.string,
};

TemplatePreview.defaultProps = {
  message: '',
  view: 'home',
  target: 'Android',
};

export default TemplatePreview;
