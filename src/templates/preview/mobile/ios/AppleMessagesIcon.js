/* eslint-disable max-len  */

import React from 'react';

const AppleMessagesIcon = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
      viewBox="0 0 60 60"
    >
      <defs>
        <clipPath id="clip-path">
          <path
            d="M39.08,0c6,0,9,0,12.28,1A12.68,12.68,0,0,1,59,8.64c1,3.24,1,6.26,1,12.28V39.08c0,6,0,9-1,12.28A12.68,12.68,0,0,1,51.36,59c-3.24,1-6.26,1-12.28,1H20.92c-6,0-9,0-12.28-1A12.72,12.72,0,0,1,1,51.36C0,48.12,0,45.1,0,39.08V20.92c0-6,0-9,1-12.28A12.72,12.72,0,0,1,8.64,1c3.24-1,6.26-1,12.28-1Z"
            fill="none"
            clipRule="evenodd"
          />
        </clipPath>
        <clipPath id="clip-path-2">
          <rect width="60" height="60" fill="none" />
        </clipPath>
        <clipPath id="clip-path-6">
          <path
            d="M18.5,44.81C12.2,41.6,8,35.83,8,29.25,8,19.17,17.85,11,30,11s22,8.17,22,18.25S42.15,47.5,30,47.5a26.15,26.15,0,0,1-4.29-.35,7,7,0,0,0-3.21.39C21,48,18.68,50,15.5,50c-.18,0,3-2.56,3-4.5Z"
            fill="none"
            clipRule="evenodd"
          />
        </clipPath>
      </defs>
      <g id="Layer_2" data-name="Layer 2">
        <g id="Home_Screen" data-name="Home Screen">
          <g id="Apps">
            <g id="Messages">
              <g id="Icons_Apps_Messages" data-name="Icons/Apps/Messages">
                <g clipPath="url(#clip-path)">
                  <g clipPath="url(#clip-path-2)" opacity="0">
                    <image
                      width="250"
                      height="250"
                      transform="scale(.24)"
                      xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPoAAAD6CAYAAACI7Fo9AAAACXBIWXMAAC4jAAAuIwF4pT92AAADD0lEQVR4Xu3dsRHDMBAEMdnDXl2HK3+VwJhcIP505yJRn9/85wGu9t0dAOcTOgQIHQKEDgFChwChQ4DQIUDoECB0CBA6BAgdAoQOAUKHAKFDgNAhQOgQIHQIEDoECB0ChA4BQocAoUOA0CFA6BAgdAhY8/hRC9zOokOA0CFA6BAgdAgQOgQIHQKEDgFChwChQ4DQIUDoECB0CBA6BAgdAoQOAcvn6HA/iw4BQocAoUOA0CFA6BAgdAgQOgQIHQKEDgFChwChQ4DQIUDoECB0CBA6BAgdAtZ4eQKuZ9EhQOgQIHQIEDoECB0ChA4BQocAoUOA0CFA6BAgdAgQOgQIHQKEDgFChwChQ4DQIUDoECB0CBA6BAgdAoQOAUKHAKFDgNAhQOgQIHQIEDoECB0ChA4BQocAoUOA0CFA6BAgdAgQOgQIHQKEDgFChwChQ4DQIUDoECB0CBA6BAgdAoQOAUKHAKFDgNAhYM3M7gY4nEWHAKFDgNAhQOgQIHQIEDoECB0ChA4BQocAoUOA0CFA6BAgdAgQOgQIHQKEDgFChwChQ4DQIUDoECB0CBA6BAgdAoQOAUKHAKFDgNAhQOgQIHQIEDoECB0ChA4BQocAoUOA0CFA6BAgdAgQOgQIHQKEDgFChwChQ4DQIUDoECB0CBA6BAgdAoQOAUKHgDUzuxvgcBYdAoQOAUKHAKFDgNAhQOgQIHQIEDoECB0ChA4BQocAoUOA0CFA6BAgdAgQOgQIHQKEDgFChwChQ4DQIUDoECB0CBA6BAgdAoQOAUKHAKFDgNAhQOgQIHQIEDoECB0C1szsboDDWXQIEDoECB0ChA4BQocAoUOA0CFA6BAgdAgQOgQIHQKEDgFChwChQ4DQIUDoECB0CBA6BAgdAoQOAUKHAKFDgNAhQOgQIHQIEDoECB0ChA4BQocAoUOA0CFA6BCwZncBHM+iQ4DQIUDoECB0CBA6BAgdAoQOAUKHAKFDgNAhQOgQIHQIEDoECB0ChA4BQoeA9YynJ+B2Fh0ChA4BQocAoUOA0CFA6BDwAsoJFdkp16/wAAAAAElFTkSuQmCC"
                    />
                  </g>
                  <g clipPath="url(#clip-path-2)">
                    <image
                      width="250"
                      height="250"
                      transform="scale(.24)"
                      xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPoAAAD6CAYAAACI7Fo9AAAACXBIWXMAAC4jAAAuIwF4pT92AAADi0lEQVR4Xu3dsXFDMQxEQdqDXl2HK4dKcGy83Vgp53DiJ/n1s7/7gNO+//oB8P/NPoEO1411DvdJdAjQ0SHAQoeA2TW6w3USHQIsdAgwukOARIcAiQ4BEh0CRp7DffOM7nCeRIcAHR0C5jm9BueNig73SXQI0NEhwL/uEGAfHQIkOgT4Mw4CbK9BgESHANtrEGB0hwCjOwRIdAjQ0SHAI4sQ4NlkCJDoEKCjQ4DtNQiwvQYBEh0CdHQIcPEEBLhKCgIkOgTo6BDgX3cIsI8OARIdAnR0CPCvOwTYR4cAiQ4BOjoEzBrd4TyJDgESHQIkOgRY6BBgdIcAiQ4BFjoEeKkFAry9BgESHQJ0dAhweg0CnEeHAIkOATo6BLgFFgLc6w4BEh0CdHQIcEwVAiQ6BEh0CJDoECDRIUCiQ4CFDgEunoAAV0lBgESHAB0dAix0CHDDDAS4Mw4CJDoE6OgQ4IYZCHBnHARIdAjQ0SHA6A4BRncIkOgQoKNDgNEdAozuECDRIUBHhwCn1yDAeXQIkOgQoKNDgOueIcADDhAg0SFAR4cAX8ZBgG/dIUCiQ4CODgFGdwgwukOARIcAHR0CfBkHAb51hwCJDgE6OgRY6BAwa38NzpPoEGChQ4DRHQJsr0GA0R0CLHQI0NEhQKJDgIUOAf51hwDHVCFAokOAjg4BLoeEANc9Q4BEhwAdHQKM7hBgdIcAiQ4BOjoE+DIOAnzrDgESHQJ0dAgYeQ73zbORDudJdAjQ0SHAl3EQ4Ft3CJDoEKCjQ4DRHQKM7hAg0SFAR4cAozsEGN0hQKJDgI4OAU6vQYDz6BAg0SFAR4cACx0CXPcMAR5wgACJDgE6OgRY6BAw64MZOE+iQ4BEhwCJDgESHQIkOgS4YQYC3BkHARIdAnR0CHDDDAS4Mw4CJDoE6OgQ4OIJCHCVFARIdAjQ0SHAQocAx1QhQKJDgIUOAUZ3CJDoECDRIUCiQ4DTaxDgPDoESHQI0NEhwC2wEOBedwiQ6BCgo0OAf90hwD46BEh0CNDRIcC/7hBgHx0CJDoE6OgQ4KUWCPD2GgRIdAjQ0SHA9hoE2F6DAIkOATo6BBjdIcCTTBBgdIcAF09AgKukIECiQ4CODgG21yDAl3EQINEhQEeHAP+6Q4B9dAiQ6BCgo0OA02sQINEhQKJDgESHAAsdAozuECDRIcBChwCPLEKAZ5MhQKJDwAelFxggweK7lAAAAABJRU5ErkJggg=="
                    />
                  </g>
                </g>
                <g clipPath="url(#clip-path-6)">
                  <rect x="3" y="6" width="54" height="49" fill="#fff" />
                </g>
              </g>
            </g>
          </g>
        </g>
      </g>
    </svg>
  );
};

export default AppleMessagesIcon;
