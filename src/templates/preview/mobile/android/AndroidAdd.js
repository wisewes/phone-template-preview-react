import React from 'react';

const AndroidAdd = () => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 56 56">
      <g id="Layer_2" data-name="Layer 2">
        <g id="Layer_1-2" data-name="Layer 1">
          <g id="Messenger">
            <g id="Button">
              <circle id="Oval" cx="28" cy="28" r="28" fill="#1a73e8" />
              <g id="Add">
                <path
                  id="Shape"
                  d="M27,21v6H21v2h6v6h2V29h6V27H29V21Z"
                  fill="#fff"
                  fillRule="evenodd"
                />
              </g>
            </g>
          </g>
        </g>
      </g>
    </svg>
  );
};

export default AndroidAdd;
