/* eslint-disable max-len */

import React from 'react';

const AndroidNotificationTitle = () => {
  return (
    <svg
      width="50px"
      height="17px"
      viewBox="0 0 140 17"
      version="1.1"
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
    >
      <g
        id="Symbols"
        stroke="none"
        strokeWidth="1"
        fill="none"
        fillRule="evenodd"
      >
        <g
          id="ui/lock_screen/notification"
          transform="translate(-13.000000, -12.000000)"
        >
          <g id="Group-4" transform="translate(0.500000, 0.058373)">
            <g id="Group-5" transform="translate(13.117175, 12.000000)">
              <text
                id="Messages"
                fontFamily="Roboto-Regular, Roboto"
                fontSize="12"
                fontWeight="normal"
                fill="#5E5E5E"
              >
                <tspan x="24" y="13">
                  Messages
                </tspan>
              </text>
              <text
                id="now"
                fontFamily="Roboto-Regular, Roboto"
                fontSize="12"
                fontWeight="normal"
                fill="#5E5E5E"
              >
                <tspan x="94.1464466" y="13">
                  now
                </tspan>
              </text>
              <circle
                id="Oval-2"
                fill="#5E5E5E"
                fillRule="nonzero"
                cx="87"
                cy="7"
                r="1"
              />
              <polygon
                id="Shape"
                fill="#5E5E5E"
                points="137.725102 5.04785853 134.424076 8.34169294 131.12305 5.04785853 130.109009 6.06189926 134.424076 10.3769662 138.739143 6.06189926"
              />
              <g
                id="Group-10-Copy"
                transform="translate(0.044590, 0.000000)"
                fillRule="nonzero"
              >
                <path
                  d="M0,0.728554396 L13.8176594,0.728554396 C14.3699441,0.728554396 14.8176594,1.17626965 14.8176594,1.7285544 L14.8176594,12 C14.8176594,12.5522847 14.3699441,13 13.8176594,13 L3.3633457,13 C2.81106095,13 2.3633457,12.5522847 2.3633457,12 L2.3633457,3.47847095 L0,0.728554396 Z"
                  id="Path-12"
                  fill="#3861CA"
                />
                <polygon
                  id="Path-13"
                  fill="#FFFFFF"
                  points="4.37684324 4.59951236 4.37684324 3.44951236 12.7490316 3.44951236 12.7490316 4.59951236"
                />
                <polygon
                  id="Path-13-Copy"
                  fill="#FFFFFF"
                  points="4.37684324 7.59951236 4.37684324 6.44951236 12.7490316 6.44951236 12.7490316 7.59951236"
                />
                <polygon
                  id="Path-13-Copy-2"
                  fill="#FFFFFF"
                  points="4.37684324 10.5995124 4.37684324 9.44951236 8.56077936 9.44951236 8.56077936 10.5995124"
                />
              </g>
            </g>
          </g>
        </g>
      </g>
    </svg>
  );
};

export default AndroidNotificationTitle;
