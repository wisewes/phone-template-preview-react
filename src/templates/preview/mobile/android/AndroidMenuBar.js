/* eslint-disable max-len */

import React from 'react';

const AndroidMenuBar = (props) => {
  return (
    <svg
      width="378px"
      height="16px"
      viewBox="0 0 378 16"
      version="1.1"
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
    >
      <defs>
        <rect id="path-1" x="0" y="0" width="412" height="30" />
      </defs>
      <g
        id="Symbols"
        stroke="none"
        strokeWidth="1"
        fill="none"
        fillRule="evenodd"
      >
        <g
          id="ui/status_bar/white"
          transform="translate(-19.000000, -10.000000)"
        >
          <g transform="translate(0.000000, 0.000000)">
            <mask id="mask-2" fill="white">
              <use xlinkHref="#path-1" />
            </mask>
            <g id="Rectangle-18" fillRule="nonzero" />
            <g
              id="ui/status_bar/time"
              strokeWidth="1"
              fillRule="evenodd"
              mask="url(#mask-2)"
              fontFamily="Roboto-Regular, Roboto"
              fontSize="14"
              fontWeight="normal"
              letterSpacing="-0.2027586"
            >
              <g transform="translate(18.000000, 8.000000)" id="3:19">
                <text>
                  <tspan
                    x="0"
                    y="15"
                    fill={props.color === 'white' ? '#FFF' : '#4A4A4A'}
                  >
                    Carrier
                  </tspan>
                </text>
              </g>
            </g>
            <text
              id="59%"
              mask="url(#mask-2)"
              fontFamily="Roboto-Regular, Roboto"
              fontSize="14"
              fontWeight="normal"
              letterSpacing="-0.2027586"
              fill={props.color === 'white' ? '#FFF' : '#4A4A4A'}
            >
              <tspan x="372" y="23">
                99%
              </tspan>
            </text>
            <g
              id="ui/status_bar/icons/white"
              mask="url(#mask-2)"
              fillRule="evenodd"
              strokeWidth="1"
            >
              <g transform="translate(282.000000, 9.000000)">
                <g
                  id="ic_signal_wifi_4_bar-copy"
                  transform="translate(39.000000, 0.000000)"
                >
                  <g id="Icon-24px">
                    <path
                      d="M7.50625,13.43125 L14.775,4.375 C14.49375,4.1625 11.69375,1.875 7.5,1.875 C3.3,1.875 0.50625,4.1625 0.225,4.375 L7.49375,13.43125 L7.5,13.4375 L7.50625,13.43125 L7.50625,13.43125 Z"
                      id="Shape"
                      fill={props.color === 'white' ? '#FFF' : '#4A4A4A'}
                    />
                    <polygon id="Shape" points="0 0 15 0 15 15 0 15" />
                  </g>
                </g>
                <g
                  id="ic_signal_cellular_4_bar-copy"
                  transform="translate(55.000000, 0.000000)"
                >
                  <g id="Icon-24px">
                    <polygon
                      id="Shape"
                      fill={props.color === 'white' ? '#FFF' : '#4A4A4A'}
                      points="1.25 13.75 13.75 13.75 13.75 1.25"
                    />
                    <polygon id="Shape" points="0 0 15 0 15 15 0 15" />
                  </g>
                </g>
                <g
                  id="ic_vibration-copy"
                  transform="translate(18.000000, 0.000000)"
                >
                  <g id="Icon-24px">
                    <polygon id="Shape" points="0 0 15 0 15 15 0 15" />
                    <path
                      d="M0,9.375 L1.25,9.375 L1.25,5.625 L0,5.625 L0,9.375 L0,9.375 Z M1.875,10.625 L3.125,10.625 L3.125,4.375 L1.875,4.375 L1.875,10.625 L1.875,10.625 Z M13.75,5.625 L13.75,9.375 L15,9.375 L15,5.625 L13.75,5.625 L13.75,5.625 Z M11.875,10.625 L13.125,10.625 L13.125,4.375 L11.875,4.375 L11.875,10.625 L11.875,10.625 Z M10.3125,1.875 L4.6875,1.875 C4.16875,1.875 3.75,2.29375 3.75,2.8125 L3.75,12.1875 C3.75,12.70625 4.16875,13.125 4.6875,13.125 L10.3125,13.125 C10.83125,13.125 11.25,12.70625 11.25,12.1875 L11.25,2.8125 C11.25,2.29375 10.83125,1.875 10.3125,1.875 L10.3125,1.875 Z M10,11.875 L5,11.875 L5,3.125 L10,3.125 L10,11.875 L10,11.875 Z"
                      id="Shape"
                      fill={props.color === 'white' ? '#FFF' : '#4A4A4A'}
                    />
                  </g>
                </g>
                <g id="ic_bluetooth-copy">
                  <g id="Icon-24px">
                    <polygon id="Shape" points="0 0 15 0 15 15 0 15" />
                    <path
                      d="M11.06875,4.81875 L7.5,1.25 L6.875,1.25 L6.875,5.99375 L4.00625,3.125 L3.125,4.00625 L6.61875,7.5 L3.125,10.99375 L4.00625,11.875 L6.875,9.00625 L6.875,13.75 L7.5,13.75 L11.06875,10.18125 L8.38125,7.5 L11.06875,4.81875 L11.06875,4.81875 Z M8.125,3.64375 L9.3,4.81875 L8.125,5.99375 L8.125,3.64375 L8.125,3.64375 Z M9.3,10.18125 L8.125,11.35625 L8.125,9.00625 L9.3,10.18125 L9.3,10.18125 Z"
                      id="Shape"
                      fill={props.color === 'white' ? '#FFF' : '#4A4A4A'}
                    />
                  </g>
                </g>
              </g>
            </g>
            <g
              id="ic_battery_full-copy"
              strokeWidth="1"
              fillRule="evenodd"
              mask="url(#mask-2)"
            >
              <g transform="translate(355.000000, 9.000000)" id="Icon-24px">
                <g>
                  <polygon id="Shape" points="0 0 15 0 15 15 0 15" />
                  <path
                    d="M9.79375,2.5 L8.75,2.5 L8.75,1.25 L6.25,1.25 L6.25,2.5 L5.20625,2.5 C4.75,2.5 4.375,2.875 4.375,3.33125 L4.375,12.9125 C4.375,13.375 4.75,13.75 5.20625,13.75 L9.7875,13.75 C10.25,13.75 10.625,13.375 10.625,12.91875 L10.625,3.33125 C10.625,2.875 10.25,2.5 9.79375,2.5 L9.79375,2.5 Z"
                    id="Shape"
                    fill={props.color === 'white' ? '#FFF' : '#4A4A4A'}
                  />
                </g>
              </g>
            </g>
          </g>
        </g>
      </g>
    </svg>
  );
};

export default AndroidMenuBar;
