/* eslint-disable max-len  */

import React from 'react';

const AndroidSubMenuBar = (props) => {
  return (
    <svg
      width="360px"
      height="25px"
      viewBox="0 0 360 56"
      version="1.1"
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
    >
      <g
        id="Symbols"
        stroke="none"
        strokeWidth="1"
        fill="none"
        fillRule="evenodd"
      >
        <g
          id="Elements-/-Toolbars-/-#7"
          transform="translate(0.000000, -24.000000)"
        >
          <g id="#7">
            <g id="Toolbar">
              <g
                id="Toolbar-contents"
                transform="translate(0.000000, 24.000000)"
              >
                <polygon
                  id="Bounds"
                  fill="#F8F8F8"
                  points="360 0 0 0 0 56 360 56"
                />
                <g
                  id="Icons-/-Navigation-24px-/-White-/-More"
                  transform="translate(332.000000, 16.000000)"
                >
                  <g id="More">
                    <rect id="Bounds" x="0" y="0" width="12" height="24" />
                    <path
                      d="M6,8 C7.1,8 8,7.1 8,6 C8,4.9 7.1,4 6,4 C4.9,4 4,4.9 4,6 C4,7.1 4.9,8 6,8 L6,8 Z M6,10 C4.9,10 4,10.9 4,12 C4,13.1 4.9,14 6,14 C7.1,14 8,13.1 8,12 C8,10.9 7.1,10 6,10 L6,10 Z M6,16 C4.9,16 4,16.9 4,18 C4,19.1 4.9,20 6,20 C7.1,20 8,19.1 8,18 C8,16.9 7.1,16 6,16 L6,16 Z"
                      id="Shape"
                      fill="#000000"
                      opacity="0.539999962"
                    />
                  </g>
                </g>
                <text
                  id="User-Experience-Desi"
                  fontFamily="Roboto-Medium, Roboto"
                  fontSize="20"
                  fontWeight="400"
                  fill="#7D7D7D"
                >
                  <tspan x="72" y="36">
                    {props.number}
                  </tspan>
                </text>
                <g
                  id="Icons-/-Navigation-24px-/-White-/-Back-Arrow"
                  transform="translate(16.000000, 16.000000)"
                >
                  <g id="Back">
                    <rect id="Bounds" x="0" y="0" width="24" height="24" />
                    <polygon
                      id="Shape"
                      fill="#000000"
                      opacity="0.539999962"
                      points="20 11 7.8 11 13.4 5.4 12 4 4 12 12 20 13.4 18.6 7.8 13 20 13"
                    />
                  </g>
                </g>
              </g>
            </g>
          </g>
        </g>
      </g>
    </svg>
  );
};

export default AndroidSubMenuBar;
