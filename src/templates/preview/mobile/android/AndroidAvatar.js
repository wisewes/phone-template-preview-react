import React from 'react';

export const AndroidAvatar = () => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40">
      <g id="Layer_2" data-name="Layer 2">
        <g id="Layer_1-2" data-name="Layer 1">
          <g id="Notification_Panel" data-name="Notification Panel">
            <g id="_3-2" data-name="3-2">
              <g id="Avatar-2">
                <circle id="Oval-3" cx="20" cy="20" r="20" fill="#e91e63" />
                <path
                  id="Icon-3"
                  d="M20.51,8a4.82,4.82,0,1,1-4.79,4.85v0A4.82,4.82,0,0,1,20.51,8ZM30.88,26.5c-1.6-3.21-5.71-5.5-10.52-5.5s-8.92,2.29-10.52,5.5c1.6,3.21,5.71,5.5,10.52,5.5S29.28,29.71,30.88,26.5Z"
                  style={{
                    isolation: 'isolate',
                  }}
                  fill="#fff"
                  fillRule="evenodd"
                  opacity="0.5"
                />
              </g>
            </g>
          </g>
        </g>
      </g>
    </svg>
  );
};

export default AndroidAvatar;
