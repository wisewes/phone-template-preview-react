import React from 'react';
import './App.css';
import 'semantic-ui-css/semantic.min.css'
import Layout from './templates/Layout';

function App() {
  return (
    <div className="app">
      <Layout />
    </div>
  );
}

export default App;
