/* The following are the various regular expressions used to identify the text type: */
// regex for gsm from stack overflow
export const REGEX__GSM = new RegExp(
  '^[A-Za-z0-9 \\r\\n@£$¥èéùìòÇØøÅå\u0394_\u03A6\u0393\u039B\u03A9\u03A0\u03A8\u03A3\u0398\u039EÆæßÉ!"#$%&\'()*+,\\-./:;<=>?¡ÄÖÑÜ§¿äöñüà^{}\\\\\\[~\\]|\u20AC]*$'
);
// regex for gsm characters that count as 2 characters
export const REGEX__GSM2 = new RegExp('[~€\\^{}\\\\[\\]\\|]', 'g');
// regex for latin-1 characters from https://gist.github.com/leodutra/3044325#file-latin1-letter-regex-js + gsm characters
const latin1 =
  '^[\u00C0-\u00ffA-Za-z0-9 \\r\\n@£$¥èéùìòÇØøÅå\u0394_\u03A6\u0393\u039B\u03A9\u03A0\u03A8\u03A3\u0398\u039EÆæßÉ!"#$%&\'()*+,\\-./:;<=>?¡ÄÖÑÜ§¿äöñüà^{}\\\\\\[~\\]|\u20AC]*$';
export const REGEX__LATIN1_GSM = new RegExp(latin1);
// regex for unicode
export const REGEX__UNICODE = new RegExp('[\u0000-\u00ff]', 'g'); //eslint-disable-line
export const REGEX__URL_HTTP_REQ = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,4}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/;
export const REGEX__CALLSID = /[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}/;